<div class="modal fade email-template" data-editor-id=".<?php echo 'tinymce-'.$lead->id; ?>" id="lead_email_send_to_client_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<?php echo form_open('admin/leads/send_to_email/'.$lead->id); ?>
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close close-send-template-modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">
					<span class="edit-title">Send email to client</span>
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
                            <?php
                                $value = isset($lead->email) ? $lead->email : '';
                                echo render_input('email','lead_add_edit_email',$value,'email',array('readonly'=>'readonly'));
                            ?>
						</div>
						<?php echo render_input('cc','CC'); ?>

                        <?php
                        $options = isset($lead_email_templates) ? $lead_email_templates : array();
                        echo render_select('template_name',$options,array('emailtemplateid','name'),'Select template','',array('required'=>'required'));
                        ?>
                        <hr />
                        <?php echo render_input('subject','Subject'); ?>
                        <h5 class="bold"><?php echo _l('estimate_send_to_client_preview_template'); ?></h5>
						<hr />
						<?php
                            $value = isset($template->message) ? $template->message : '';
                            echo render_textarea('email_template_custom','',$value,array(),array(),'','tinymce-'.$lead->id);
                        ?>
					</div>
				</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default close-send-template-modal"><?php echo _l('close'); ?></button>
						<button type="submit" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info disabled email_send_btn"><?php echo _l('send'); ?></button>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
    </div>
</div>
<script>
    //email modal script
    // Show send to email estimate modal
    $("body").on('click', '.lead_email_send_to_client', function(e) {
        e.preventDefault();
        $('#lead_email_send_to_client_modal').modal('show');
    });

    // Send template modal custom close function causing problems if is on pipeline view
    $("body").on('click', '.close-send-template-modal', function() {
        $('#lead_email_send_to_client_modal').modal('hide');
    });

    $("body").on('change','#template_name',function (e) {
        var sel_val = $( "#template_name option:selected").val();
        if(sel_val != '')
        {
            $('button.email_send_btn').removeClass('disabled');
        }
        else {
            $('button.email_send_btn').addClass('disabled');
        }
    })

    $("body").on('change','#template_name',function (e) {
        var id = $(this).val();
        $.ajax({
            type: "post",
            data: {
                id:id
            },
            url: admin_url+'/emails/get_email_template_by_id'
        }).done(function(response) {
            response = JSON.parse(response);
            if(!response.error)
            {
                tinymce.activeEditor.setContent(response.result.message);
                $("[name='subject']").val(response.result.subject);
            } else {
                alert("Error in fetching template!")
            }
        }).fail(function(error) {
            alert_float('danger', JSON.parse(error.responseText));
        });

    })
</script>
