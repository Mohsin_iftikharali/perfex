<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php init_head(); ?>
<div id="wrapper">
	<div class="content">
		<div class="row">
			<?php
			echo form_open($this->uri->uri_string(),array('id'=>'invoice-form','class'=>'_transaction_form invoice-form'));
			if(isset($invoice)){
				echo form_hidden('isedit');
			}
			?>
			<div class="col-md-12">
				<?php $this->load->view('admin/invoices/invoice_template'); ?>
			</div>
			<?php echo form_close(); ?>
			<?php $this->load->view('admin/invoice_items/item'); ?>
		</div>
	</div>
</div>
<?php init_tail(); ?>
<script>
	$(function(){
		validate_invoice_form();
	    // Init accountacy currency symbol
	    init_currency();
	    // Project ajax search
	    init_ajax_project_search_by_customer_id();
	    // Maybe items ajax search
	    init_ajax_search('items','#item_select.ajax-search',undefined,admin_url+'items/search');

	    //installment script
        $(".invoice-form-submit").click(function (e) {
            var sum_of_inst = parseFloat($("#first_installment_amount").val()) + parseFloat($("#second_installment_amount").val());

            if(sum_of_inst > $("input[name=total]").val())
            {
                alert("Sum of installment can't be greater than total!");
                return false;
            }
        });
        $('body').on('change','#first_installment_amount',function (e) {
            var total = parseFloat($("input[name=total]").val());
            var first = parseFloat($(this).val());
            var second = total-first <= 0 ? 0 : total-first;
            $('#second_installment_amount').val(second);
        });

        $('input[name=inst_type]').click(function (e) {
            if($(this).val() == 'percentage')
            {
                $('td.first_inst').html(`<div class="row">
                          <div class="col-md-3">
                              <?php echo render_input('first_installment_amount_percentage','','0','number',['min'=>0,'max'=>100]); ?>
                          </div>
                          <div class="col-md-9">
                              <?php echo render_input('first_installment_amount','','0.00','number',['readonly'=>true]);?>
                          </div>
                      </div>`);

                $('td.second_inst').html(`<div class="row">
                          <div class="col-md-3">
                              <?php echo render_input('second_installment_amount_percentage','','0','number',['readonly'=>true]); ?>
                          </div>
                          <div class="col-md-9">
                            <?php echo render_input('second_installment_amount','','0.00','number',['readonly'=>true]);?>
                          </div>
                      </div>`);
            }
            else {
                $('td.first_inst').html(`<?php
                    echo render_input('first_installment_amount','','0.00','number');
                    ?>`);

                $('td.second_inst').html(`<?php
                    echo render_input('second_installment_amount','','0.00','number');
                    ?>`);
            }

        });

        $('body').on('change','#first_installment_amount_percentage',function (e) {
            var total = parseFloat($("input[name=total]").val());
            var first_percentage = $(this).val();
            var calculated_val = first_percentage*total/100;
            $('#first_installment_amount').val(calculated_val);
            $('#second_installment_amount_percentage').val(100-first_percentage);
            $('#second_installment_amount').val(total-calculated_val);
        });
	});
</script>
</body>
</html>
